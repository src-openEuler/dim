%global debug_package %{nil}
%define kernel_version %(ver=`rpm -qa|grep kernel-devel`;echo ${ver#*kernel-devel-})

# Redefining __spec_install_post, Causes the kernel
# module to be signed at the end of the install phase
%if 0%{?openEuler_sign_rsa}
%global __modsign_install_post \
module_path="$RPM_BUILD_ROOT/lib/modules/%{kernel_version}/extra/dim" \
sh /usr/lib/rpm/brp-ebs-sign --module $module_path/dim_core.ko ||: \
mv $module_path/dim_core.ko.sig $module_path/dim_core.ko ||: \
sh /usr/lib/rpm/brp-ebs-sign --module $module_path/dim_monitor.ko ||: \
mv $module_path/dim_monitor.ko.sig $module_path/dim_monitor.ko ||: \
%{nil}

%global __tmp_spec_install_post %{__spec_install_post}
%global __spec_install_post \
%{__tmp_spec_install_post} \
%{__modsign_install_post} \
%{nil}
%endif

Name         :  dim
Summary      :  Dynamic Integrity Measurement
Version      :  1.0.2
Release      :  10
License      :  GPL-2.0
Source0      :  %{name}-v%{version}.tar.gz
BuildRequires:  kernel-devel kernel-headers
Requires     :  kernel

Patch0001:      Limit-the-max-line-number-of-policy-and-baseline-par.patch
Patch0002:      Use-jiffies64-interface-to-set-measure-interval.patch
Patch0003:      Add-the-owner-of-file-operations.patch
Patch0004:      backport-dim-add-test-code.patch
Patch0005:      backport-fix-the-magic-number.patch
Patch0006:      backport-some-word.patch
Patch0007:      backport-update-src-common-dim_baseline.c.patch
Patch0008:      backport-fix-build-error-in-kernel-6.6.patch
Patch0009:      backport-fix-build-error.patch
Patch0010:      backport-Refactor-the-measurement-code.patch
Patch0011:      backport-Refactor-dim_core-policy-and-support-the-action-poli.patch
Patch0012:      backport-Refactor-the-dim_core-static-baseline-implement.patch
Patch0013:      backport-Support-user-process-measurement-by-ELF-parsing.patch
Patch0014:      backport-Optimize-Makefile.patch
Patch0015:      backport-Dont-queue-measurement-task-when-baseline-failed.patch
Patch0016:      backport-Add-safe-wapper-for-some-memory-and-string-functions.patch
Patch0017:      backport-Fix-potential-integer-overflow.patch
Patch0018:      backport-Add-memory-debug-in-mem_pool.patch
Patch0019:      backport-Optimize-test-framework-and-add-testcases.patch
Patch0020:      backport-Add-warpper-for-strncmp-and-strncpy.patch
Patch0021:      backport-Use-warpper-dim_vzalloc-to-avoid-false-warning.patch
Patch0022:      backport-Set-dim_core_keyring-to-NULL-when-initialize-failed.patch
Patch0023:      backport-Disable-dfx-testcase-by-default.patch
Patch0024:      backport-Support-init-function-for-measure-tasks.patch
Patch0025:      backport-Fix-calculating-ELF-memory-address.patch
Patch0026:      backport-use-fs-interface-to-set-measure-action.patch
Patch0027:      backport-fix-incorrect-indent.patch
Patch0028:      backport-add-two-interfaces-for-baseline-operations.patch
Patch0029:      backport-Add-sm3-compile-macro-and-set-the-algo-name.patch
Patch0030:      backport-Try-to-add-the-absolute-path-of-process-in-static-ba.patch
Patch0031:      backport-Fix-the-type-of-pcr.patch
Patch0032:      backport-Adapter-test-cases.patch
Patch0033:      backport-Remove-unused-symbol-in-dim_core.patch
Patch0034:      backport-dont-kill-the-init-process.patch
Patch0035:      backport-set-dim_work_queue-to-NULL-after-fail-branch.patch
Patch0037:      backport-add-missing-line-break-in-log-printing.patch
Patch0038:      backport-dont-warp-strncpy.patch
Patch0039:      backport-Fix-calculating-ELF-trampoline-address.patch
Patch0040:      backport-Fix-NULL-pointer-reference-when-kill-child-processes.patch
Patch0041:      backport-fix-double-free-in-tpm.patch
Patch0042:      backport-fix-trampoline.patch
Patch0043:      backport-Maximun-number-of-line-in-a-modification-policy.patch
Patch0044:      backport-Optimize-task-kill-and-log-the-static-baseline-when-.patch
Patch0045:      backport-fix-resource-clear-in-concurrent-scenarios.patch
Patch0046:      backport-Fix-the-issue-that-the-memory-allocation-is-too-larg.patch
Patch0047:      backport-ignore-return-value-if-the-measure-log-is-limited.patch
Patch0048:      backport-Change-the-permissions-of-the-dim-directory-to-500.patch
Patch0049:      backport-Unified-log-printing-format.patch
Patch0050:      backport-Fix-print-errors.patch
Patch0051:      backport-add-parameter-check.patch
Patch0052:      backport-Fix-deadlock-issue-in-directory-iterating.patch
Patch0053:      backport-Optimized-directory-suffix-matching.patch
Patch0054:      backport-Resolved-the-problem-that-the-jump_label_lock-isrepe.patch

%description
Dynamic Integrity Measurement

%prep
%autosetup -n %{name}-v%{version} -p1

%build
cd src
sed -i 's#/lib/modules/$(shell uname -r)/build#/lib/modules/%{kernel_version}/build#' Makefile
make EXTRA_CFLAGS+=-DDIM_HASH_SUPPORT_SM3

%install
mkdir -p $RPM_BUILD_ROOT/lib/modules/%{kernel_version}/extra/dim
install -m 600 ./src/dim_core.ko $RPM_BUILD_ROOT/lib/modules/%{kernel_version}/extra/dim
install -m 600 ./src/dim_monitor.ko $RPM_BUILD_ROOT/lib/modules/%{kernel_version}/extra/dim

%pre

%post
depmod -a `uname -r`

%preun

%postun
depmod -a

%posttrans

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(0400,root,root) /lib/modules/%{kernel_version}/extra/dim/dim_core.ko
%attr(0400,root,root) /lib/modules/%{kernel_version}/extra/dim/dim_monitor.ko

%changelog
* Wed Feb 5 2025 steven.ygui <steven_ygui@163.com> 1.0.2-10
- add signature and backport some patches

* Wed Aug 28 2024 jinlun <jinlun@huawei.com> 1.0.2-9
- Fix deadlock issue in directory iterating

* Mon Aug 19 2024 gengqihu <gengqihu2@h-partners.com> 1.0.2-8
- Enabled DIM_HASH_SUPPORT_SM3

* Mon Aug 19 2024 gengqihu <gengqihu2@h-partners.com> 1.0.2-7
- fix some bugs

* Tue Apr 16 2024 jinlun <jinlun@huawei.com> 1.0.2-6
- backport some patches

* Fri Jan 26 2024 jinlun <jinlun@huawei.com> 1.0.2-5
- The compilation error asused by the kernel upgrade is rectified.

* Mon Sep 18 2023 jinlun <jinlun@huawei.com> 1.0.2-4
- Fix the concurrent issues with removing module and accessing interfaces.

* Fri Sep 15 2023 luhuaxin <luhuaxin1@huawei.com> 1.0.2-3
- Use jiffies64 interface to set measure interval

* Thu Sep 14 2023 luhuaxin <luhuaxin1@huawei.com> 1.0.2-2
- Limit the max line number of policy and baseline parsing

* Mon Sep 4 2023 jinlun <jinlun@huawei.com> 1.0.2-1
- Init package
